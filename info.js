const li1 = document.getElementById('1');
const li2 = document.getElementById('2');
const li3 = document.getElementById('3');
const li4 = document.getElementById('4');
const li5 = document.getElementById('5');
const li6 = document.getElementById('6');
const li7 = document.getElementById('7');
const li8 = document.getElementById('8');
const li9 = document.getElementById('9');

const ifDraw = () => {
    if((li1.textContent + li2.textContent + li3.textContent + li4.textContent + li5.textContent + li6.textContent + li7.textContent + li8.textContent + li9.textContent).length === 9){
        const playAgain = document.getElementById('again');
        playAgain.innerHTML = `
            <h3 class="active">Draw !!!</h3>
            <p>Click to play again.</p>
        `;
        playAgain.addEventListener('click', () => {
            if((li1.textContent + li2.textContent + li3.textContent + li4.textContent + li5.textContent + li6.textContent + li7.textContent + li8.textContent + li9.textContent).length === 9){
                for (let i = 0; i < 9; i++){
                    lis[i].textContent = '';
                    lis[i].className = '';
                }
                playAgain.innerHTML = `
                <h3>Play with responsibility !!!</h3>
            `;
                i = 0;
            }
        });
    }
}

const addXO = function () {
    if (!isWinnerX && !isWinnerO && isProgress){
        if (!this.textContent){
            this.textContent = 'x';
            ifDraw();
            getWinner();
            setTimeout(
                addO.bind(this, 'o'),
                1000
            );
        }
    }
};

li1.addEventListener('click', addXO);
li2.addEventListener('click', addXO);
li3.addEventListener('click', addXO);
li4.addEventListener('click', addXO);
li5.addEventListener('click', addXO);
li6.addEventListener('click', addXO);
li7.addEventListener('click', addXO);
li8.addEventListener('click', addXO);
li9.addEventListener('click', addXO);