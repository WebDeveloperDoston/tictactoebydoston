let lis = document.getElementsByTagName('li');

let i = 0;
let isWinnerX = false;
let isWinnerO = false;
let isProgress = true;

const getWinner = () =>{
    if ((lis[0].textContent + lis[1].textContent + lis[2].textContent) === 'xxx'){
        lis[0].className = 'winner';
        lis[1].className = 'winner';
        lis[2].className = 'winner';
        isWinnerX = true;
    }
    else if ((lis[3].textContent + lis[4].textContent + lis[5].textContent) === 'xxx'){
        lis[3].className = 'winner';
        lis[4].className = 'winner';
        lis[5].className = 'winner';
        isWinnerX = true;
    }
    else if ((lis[6].textContent + lis[7].textContent + lis[8].textContent) === 'xxx'){
        lis[6].className = 'winner';
        lis[7].className = 'winner';
        lis[8].className = 'winner';
        isWinnerX = true;
    }
    else if ((lis[0].textContent + lis[3].textContent + lis[6].textContent) === 'xxx'){
        lis[0].className = 'winner';
        lis[3].className = 'winner';
        lis[6].className = 'winner';
        isWinnerX = true;
    }
    else if ((lis[1].textContent + lis[4].textContent + lis[7].textContent) === 'xxx'){
        lis[1].className = 'winner';
        lis[4].className = 'winner';
        lis[7].className = 'winner';
        isWinnerX = true;
    }
    else if ((lis[2].textContent + lis[5].textContent + lis[8].textContent) === 'xxx'){
        lis[2].className = 'winner';
        lis[5].className = 'winner';
        lis[8].className = 'winner';
        isWinnerX = true;
    }
    else if ((lis[0].textContent + lis[4].textContent + lis[8].textContent) === 'xxx'){
        lis[0].className = 'winner';
        lis[4].className = 'winner';
        lis[8].className = 'winner';
        isWinnerX = true;
    }
    else if ((lis[2].textContent + lis[4].textContent + lis[6].textContent) === 'xxx'){
        lis[2].className = 'winner';
        lis[4].className = 'winner';
        lis[6].className = 'winner';
        isWinnerX = true;
    }
    
    if (isWinnerX){
        const playAgain = document.getElementById('again');
        playAgain.innerHTML = `
            <h3 class="active">You won !!!</h3>
            <p>Click to play again.</p>
        `;
        playAgain.addEventListener('click', () => {
            if (isWinnerX){
                for (let i = 0; i < 9; i++){
                    lis[i].textContent = '';
                    lis[i].className = '';
                }
                playAgain.innerHTML = `
                <h3>Play with responsibility !!!</h3>
            `;
                isWinnerX = false;
                i = 0;
            }
        });
    }
    isProgress = false;
}

const addO = o => {
    if (!isWinnerX && !isWinnerO){

        if ((lis[0].textContent + lis[1].textContent + lis[2].textContent) === 'xx'){
            if ((lis[3].textContent + lis[4].textContent + lis[5].textContent) === 'oo'){
                for (let i = 3; i < 6; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
            else if ((lis[6].textContent + lis[7].textContent + lis[8].textContent) === 'oo'){
                for (let i = 6; i < 9; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }

            else {
                for (let i = 0; i < 3; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
        }
        else if ((lis[3].textContent + lis[4].textContent + lis[5].textContent) === 'xx'){
            if ((lis[0].textContent + lis[1].textContent + lis[2].textContent) === 'oo'){
                for (let i = 0; i < 3; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
            else if ((lis[6].textContent + lis[7].textContent + lis[8].textContent) === 'oo'){
                for (let i = 6; i < 9; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }

            else {
                for (let i = 3; i < 6; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
        }
        else if ((lis[6].textContent + lis[7].textContent + lis[8].textContent) === 'xx'){
            if ((lis[3].textContent + lis[4].textContent + lis[5].textContent) === 'oo'){
                for (let i = 3; i < 6; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
            else if ((lis[0].textContent + lis[1].textContent + lis[2].textContent) === 'oo'){
                for (let i = 0; i < 3; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }

            else {
                for (let i = 6; i < 9; i++){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
        }
        else if ((lis[0].textContent + lis[3].textContent + lis[6].textContent) === 'xx'){
            if ((lis[1].textContent + lis[4].textContent + lis[7].textContent) === 'oo'){
                for (let i = 1; i < 8; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
            else if ((lis[2].textContent + lis[5].textContent + lis[8].textContent) === 'oo'){
                for (let i = 2; i < 9; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }

            else {
                for (let i = 0; i < 7; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
        }
        else if ((lis[1].textContent + lis[4].textContent + lis[7].textContent) === 'xx'){
            if ((lis[0].textContent + lis[3].textContent + lis[6].textContent) === 'oo'){
                for (let i = 0; i < 7; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
            else if ((lis[2].textContent + lis[5].textContent + lis[8].textContent) === 'oo'){
                for (let i = 2; i < 9; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
            else {
                for (let i = 1; i < 8; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
        }
        else if ((lis[2].textContent + lis[5].textContent + lis[8].textContent) === 'xx'){
            if ((lis[1].textContent + lis[4].textContent + lis[7].textContent) === 'oo'){
                for (let i = 1; i < 8; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
            else if ((lis[0].textContent + lis[3].textContent + lis[6].textContent) === 'oo'){
                for (let i = 0; i < 7; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }

            else {
                for (let i = 2; i < 9; i += 3){
                    if (!lis[i].textContent){
                        lis[i].textContent = o;
                        break;
                    }
                }
            }
        }
        else if ((lis[0].textContent + lis[4].textContent + lis[8].textContent) === 'xx'){
            for (let i = 0; i < 9; i += 4){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[2].textContent + lis[4].textContent + lis[6].textContent) === 'xx'){
            for (let i = 2; i < 7; i += 2){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[0].textContent + lis[1].textContent + lis[2].textContent) === 'oo'){
            for (let i = 0; i < 3; i++){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[3].textContent + lis[4].textContent + lis[5].textContent) === 'oo'){
            for (let i = 3; i < 6; i++){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[6].textContent + lis[7].textContent + lis[8].textContent) === 'oo'){
            for (let i = 6; i < 9; i++){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[0].textContent + lis[3].textContent + lis[6].textContent) === 'oo'){
            for (let i = 0; i < 7; i += 3){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[1].textContent + lis[4].textContent + lis[7].textContent) === 'oo'){
            for (let i = 1; i < 8; i += 3){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[2].textContent + lis[5].textContent + lis[8].textContent) === 'oo'){
            for (let i = 2; i < 9; i += 3){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[0].textContent + lis[4].textContent + lis[8].textContent) === 'oo'){
            for (let i = 0; i < 9; i += 4){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if ((lis[2].textContent + lis[4].textContent + lis[6].textContent) === 'oo'){
            for (let i = 0; i < 7; i += 2){
                if (!lis[i].textContent){
                    lis[i].textContent = o;
                    break;
                }
            }
        }
        else if (i !== 4){
            rnd = Math.floor(Math.random() * 9);
            while (lis[rnd].textContent){
                rnd = Math.floor(Math.random() * 9)
            }
        }
        else {
            for (let i = 0; i < 9; i++){
                if (!lis[i].textContent){
                    rdn = i;
                    break;
                }
            }
        }
        lis[rnd].textContent = o;
        i++;
    }

    if ((lis[0].textContent + lis[1].textContent + lis[2].textContent) === 'ooo'){
        lis[0].className = 'winner';
        lis[1].className = 'winner';
        lis[2].className = 'winner';
        isWinnerO = true;
    }
    else if ((lis[3].textContent + lis[4].textContent + lis[5].textContent) === 'ooo'){
        lis[3].className = 'winner';
        lis[4].className = 'winner';
        lis[5].className = 'winner';
        isWinnerO = true;
    }
    else if ((lis[6].textContent + lis[7].textContent + lis[8].textContent) === 'ooo'){
        lis[6].className = 'winner';
        lis[7].className = 'winner';
        lis[8].className = 'winner';
        isWinnerO = true;
    }
    else if ((lis[0].textContent + lis[3].textContent + lis[6].textContent) === 'ooo'){
        lis[0].className = 'winner';
        lis[3].className = 'winner';
        lis[6].className = 'winner';
        isWinnerO = true;
    }
    else if ((lis[1].textContent + lis[4].textContent + lis[7].textContent) === 'ooo'){
        lis[1].className = 'winner';
        lis[4].className = 'winner';
        lis[7].className = 'winner';
        isWinnerO = true;
    }
    else if ((lis[2].textContent + lis[5].textContent + lis[8].textContent) === 'ooo'){
        lis[2].className = 'winner';
        lis[5].className = 'winner';
        lis[8].className = 'winner';
        isWinnerO = true;
    }
    else if ((lis[0].textContent + lis[4].textContent + lis[8].textContent) === 'ooo'){
        lis[0].className = 'winner';
        lis[4].className = 'winner';
        lis[8].className = 'winner';
        isWinnerO = true;
    }
    else if ((lis[2].textContent + lis[4].textContent + lis[6].textContent) === 'ooo'){
        lis[2].className = 'winner';
        lis[4].className = 'winner';
        lis[6].className = 'winner';
        isWinnerO = true;
    }

    if (isWinnerO){
        const playAgain = document.getElementById('again');
        playAgain.innerHTML = `
            <h3 class="active">You lost !!!</h3>
            <p>Click to play again.</p>
        `;
        playAgain.addEventListener('click', () => {
            if (isWinnerO){
                for (let i = 0; i < 9; i++){
                    lis[i].textContent = '';
                    lis[i].className = '';
                }
                isWinnerO = false;
                playAgain.innerHTML = `
                <h3>Play with responsibility !!!</h3>
            `;
                i = 0;
            }
        });
    }
    isProgress = true;
};